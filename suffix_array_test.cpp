
#include <gtest/gtest.h>
#include "suffix_array/suffix.cpp"

TEST(TestFunctions, TestSuffixArray) {
  std::vector<int> sa, answ;
  buildSuffixArray("abracadabra", sa);
  answ = {10, 7, 0, 3, 5, 8, 1, 4, 6, 9, 2};
  for (int i = 0; i < answ.size(); ++i) {
    EXPECT_EQ(sa[i], answ[i]);
  }
  buildSuffixArray("abaab", sa);
  answ = {2, 3, 0, 4, 1};
  for (int i = 0; i < answ.size(); ++i) {
    EXPECT_EQ(sa[i], answ[i]);
  }
}

TEST(TestFunctions, TestLCP) {
  std::vector<int> sa, lcp, answ;
  std::string s = "banana";
  buildSuffixArray(s, sa);
  buildLcp(sa, s, lcp);
  answ = {1, 3, 0, 0, 2};
  for (int i = 0; i < answ.size(); ++i)
    EXPECT_EQ(lcp[i], answ[i]);
  s = "aabaaca";
  buildSuffixArray(s, sa);
  buildLcp(sa, s, lcp);
  answ = {1, 2, 1, 1, 0, 0};
  for (int i = 0; i < answ.size(); ++i)
    EXPECT_EQ(lcp[i], answ[i]);
}

TEST(TestAlgo, TestAlgo) {
  EXPECT_EQ(calculateSubstrings("I"), 1);
  EXPECT_EQ(calculateSubstrings("Love"), 10);
  EXPECT_EQ(calculateSubstrings("this"), 10);
  EXPECT_EQ(calculateSubstrings("homework"), 35);
  EXPECT_EQ(calculateSubstrings("so"), 3);
  EXPECT_EQ(calculateSubstrings("much"), 10);
  EXPECT_EQ(calculateSubstrings("desire"), 20);
  EXPECT_EQ(calculateSubstrings("bed"), 6);
  EXPECT_EQ(calculateSubstrings("sleep"), 14);
}