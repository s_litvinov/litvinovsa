#include <iostream>
#include <vector>
#include <algorithm>

const int alphabetSize = 26; // size of the alphabet
void buildSuffixArray(const std::string s, std::vector<int> &suff_arr) {
  int n = s.length();
  std::vector<int> order (n);
  for(int i = 0; i < n; ++i)
    order[i] = n - 1 - i;
  std::stable_sort(order.begin(), order.end(), [&s](int a, int b){
    return s[a] < s[b];
  });
  suff_arr.resize(n);
  std::vector<int> classes(n);
  for(int i = 0; i < n; ++i){
    suff_arr[i] = order[i];
    classes[i] = s[i];
  }
  for (int len = 1; len < n; len *= 2) {
    std::vector<int> c(n);
    std::copy(classes.begin(), classes.end(), c.begin());
    for (int i = 0; i < n; ++i)
      classes[suff_arr[i]] = i > 0
                                 && c[suff_arr[i - 1]] == c[suff_arr[i]]
                                 && suff_arr[i - 1] + len < n
                                 && c[suff_arr[i - 1] + len / 2] == c[suff_arr[i] + len / 2]
                             ? classes[suff_arr[i - 1]] : i;

    std::vector<int> cntr(n);
    for (int i = 0; i < n; ++i)
      cntr[i] = i;
    std::vector<int> s (n);
    std::copy(suff_arr.begin(), suff_arr.end(), s.begin());
    for(int i = 0; i < n; ++i){
      int s1 = s[i] - len;
      if(s1 >= 0)
        suff_arr[cntr[classes[s1]]++]= s1;
    }
  }
}

void buildLcp(std::vector<int> &suff_a, const std::string &s, std::vector<int> &lcp) {
  int n = s.length();
  lcp.resize(n);
  std::vector<int> rank(n);
  for (int i = 0; i < n; ++i)
    rank[suff_a[i]] = i;
  for (int i = 0, k = 0; i < n; ++i) {
    if (k > 0) k--;
    if (rank[i] == n - 1)
    {
      lcp[n - 1] = -1, k = 0;
      continue;
    }
    int j = suff_a[rank[i] + 1];
    while (std::max(i + k, j + k) < s.length() && s[i + k] == s[j + k])
      k++;
    lcp[rank[i]] = k;
  }
  lcp.shrink_to_fit();
}
int calculateSubstrings(const std::string &s) {
  std::vector<int> p;
  buildSuffixArray(s, p);
  std::vector<int> lcp;
  buildLcp(p, s, lcp);
  int ans = 0;
  for (int i = 0; i < s.length(); ++i) {
    ans += s.length() - p[i];
    if (i < s.length() - 1)
      ans -= lcp[i];
  }
  return ans;
}
#ifndef GTEST_TEST
int main (){
  std::string s;
  std::cin >> s;
  std::cout << calculateSubstrings(s) << std::endl;
}
#endif