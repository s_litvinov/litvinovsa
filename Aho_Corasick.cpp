#include <gtest/gtest.h>
//#include "Aho-Corasick/Bohr.h"
#include "Aho-Corasick/Bohr.cpp"

TEST(TestAho, TestParsing) {
  Bohr bohr;
  EXPECT_EQ(bohr.split_pattern("?"), 0);
  EXPECT_EQ(bohr.split_pattern("?a"), 1);
  EXPECT_EQ(bohr.split_pattern("a?a"), 2);
  EXPECT_EQ(bohr.split_pattern("ab??aba"), 2);
}

TEST(TestAho, TestResult1) {
  Bohr bohr;
  std::string answ = bohr.find_all_pos("ababacaba", "ab??aba");
  EXPECT_EQ(answ, "2 ");
}
TEST(TestAho, TestResultBigPattern) {
  Bohr bohr;
  std::string answ = bohr.find_all_pos("ababacaba", "ab??aba????arhearh");
  EXPECT_EQ(answ, "");
}
TEST(TestAho, TestResult2) {
  Bohr bohr;
  std::string answ = bohr.find_all_pos("ababacabaabcarhbac", "a?a");
  EXPECT_EQ(answ, "0 2 4 6 ");
}
TEST(TestAho, TestResultAllJoker) {
  Bohr bohr;
  std::string answ = bohr.find_all_pos("ababacabaeathjoneobnetihgboib", "?????");
  EXPECT_EQ(answ, "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 ");
}
