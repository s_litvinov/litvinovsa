#include <iostream>
#include <vector>
#include <fstream>

std::vector<int> PrefixBuildFromZ(const std::vector<int> &z) {
  std::vector<int> prefix;
  prefix.assign(z.size(), 0);
  for(int i = 1; i < z.size(); ++i)
    for(int j = z[i] - 1; j >= 0; --j){
      if(prefix[i + j] > 0)
        break;
      else
        prefix[i + j] = j + 1;
    }
  return prefix;
}

std::string buildFromPrefix(const std::vector<int> &p) {
  char max = 0;
  std::string str = "a";
  for(int i = 1; i < p.size(); ++i){
    if (p[i] == 0){
      int j = i;
      max = str[p[j - 1]] + 1 ;
      while(p[j - 1] > 0){
        if( str[p[j - 1]] + 1 > max ){
          max = str[p[j -1]] + 1;
        }
        j = p[j - 1];
      }
      str+= max;
    }
    else
      str += str[p[i] - 1];
  }
  return str;
}
#ifndef GTEST_TEST
int main(int argc, char *argv[]){
  std::vector<int> input;
  std::ifstream fin("input.txt");
  int t;
  while(fin >> t)
    input.push_back(t);
  std::cout << buildFromPrefix(PrefixBuildFromZ(input)) << std::endl;
}
#endif