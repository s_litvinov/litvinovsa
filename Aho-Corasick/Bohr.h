//
// Created by stanislav on 16.12.16.
//

#ifndef HOMEWORK3_BOHR_H
#define HOMEWORK3_BOHR_H

#include <vector>
#include <cstring>
#include <string>

const int alphabetSize = 26; //k - size of ABC
struct bohr_vertex {
  int next_vertex[alphabetSize],
      patt_num,
      suff_link,
      suff_flink,
      auto_move[alphabetSize],
      parent;
  std::vector<int> beg_of_patt;
  char symb;
  bool flag;
};

class Bohr {
 public:
  Bohr();
  std::vector<int> unmask;

  std::string find_all_pos(const std::string &s, const std::string &p);
  void add_str_to_bohr(const std::string &s, int pos);
  int split_pattern(const std::string &p);

 private:
  std::vector<bohr_vertex> bohr;
  std::vector<std::string> pattern;

  int get_auto_move(int v, char ch);
  bohr_vertex make_bohr_vertex(int p, char c);
  int get_suff_link(int v);
  int get_suff_flink(int v);
  void check(int v, int i);
};

#endif //HOMEWORK3_BOHR_H
