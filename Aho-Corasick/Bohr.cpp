//
// Created by stanislav on 16.12.16.
//

#include <iostream>
#include "Bohr.h"

Bohr::Bohr() {
  bohr.push_back(make_bohr_vertex(0, 0));
}
bohr_vertex Bohr::make_bohr_vertex(int p, char c) {
  bohr_vertex v;
  memset(v.next_vertex, 255, sizeof(v.next_vertex));
  memset(v.auto_move, 255, sizeof(v.auto_move));
  v.flag = false;
  v.parent = p;
  v.symb = c;
  v.suff_flink = v.suff_link = -1;
  return v;
}
void Bohr::add_str_to_bohr(const std::string &s, int pos) {
  int num = 0;
  for (int i = 0; i < s.length(); ++i) {
    char ch = s[i] - 'a';
    if (bohr[num].next_vertex[ch] == -1) {
      bohr.push_back(make_bohr_vertex(num, ch));
      bohr[num].next_vertex[ch] = (bohr.size() - 1);
    }
    num = bohr[num].next_vertex[ch];
  }
  bohr[num].flag = true;
  bohr[num].beg_of_patt.push_back(pos);
  pattern.push_back(s);
  bohr[num].patt_num = (pattern.size() - 1);
}
int Bohr::get_suff_link(int v) {
  if (bohr[v].suff_link == -1)
    if (v == 0 || bohr[v].parent == 0)
      bohr[v].suff_link = 0;
    else
      bohr[v].suff_link =
          get_auto_move(get_suff_link(bohr[v].parent),
                        bohr[v].symb);
  return bohr[v].suff_link;
}
int Bohr::get_auto_move(int v, char ch) {
  if (bohr[v].auto_move[ch] == -1)
    if (bohr[v].next_vertex[ch] != -1)
      bohr[v].auto_move[ch] = bohr[v].next_vertex[ch];
    else if (v == 0)
      bohr[v].auto_move[ch] = 0;
    else
      bohr[v].auto_move[ch] =
          get_auto_move(get_suff_link(v), ch);
  return bohr[v].auto_move[ch];
}
int Bohr::get_suff_flink(int v) {
  if (bohr[v].suff_flink == -1) {
    int u = get_suff_link(v);
    if (u == 0)
      bohr[v].suff_flink = 0;
    else
      bohr[v].suff_flink = (bohr[u].flag) ? u : get_suff_flink(u);
  }
  return bohr[v].suff_flink;
}
int Bohr::split_pattern(const std::string &p) {
  std::string str = "";
  int num = 0;
  for (int i = 0; i <= p.length(); ++i) {
    if ((p[i] == '?' || p[i] == 0) && str.length() != 0) {
      add_str_to_bohr(str, i - str.length());
      str = "";
      num++;
    } else if (p[i] != '?')
      str += p[i];
  }
  return num;
}
void Bohr::check(int v, int i) {
  for (int u = v; u != 0; u = get_suff_flink(u)) {
    if (bohr[u].flag) {
      for (int j = 0; j < bohr[u].beg_of_patt.size(); ++j) {
        int poss_pos = i - pattern[bohr[u].patt_num].length() - bohr[u].beg_of_patt[j];
        if (poss_pos >= 0 && poss_pos < unmask.size())
          unmask[poss_pos]++;
      }
    }
  }
}
std::string Bohr::find_all_pos(const std::string &s, const std::string &p) {
  std::string str = "";
  if (p.length() <= s.length()) {
    unmask.resize(s.length() - p.length() + 1, 0);
    split_pattern(p);
    int u = 0;
    for (int i = 0; i < s.length(); ++i) {
      u = get_auto_move(u, s[i] - 'a');
      check(u, i + 1);
    }
    for (int i = 0; i < unmask.size(); ++i)
      if (unmask[i] == pattern.size())
        str.append(std::to_string(i) + " ");
  }
  str += "\0";
  unmask.clear();
  pattern.clear();
  bohr.clear();
  return str;
}

