#include <iostream>
#include <vector>
#include <cstring>
#include <algorithm>
#include "Bohr.h"

int main() {
  Bohr bohr;
  std::string patt, text;
  std::cin >> patt >> text;
  std::cout << bohr.find_all_pos(text, patt) << std::endl;
  return 0;
}