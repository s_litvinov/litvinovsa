#include <gtest/gtest.h>
#include "KMP_algo/kmp.cpp"

TEST(Test_functions, test_prefix_func) {
  std::string s = "aabaaab";
  std::vector<int> answ = {0, 1, 0, 1, 2, 2, 3};
  std::vector<int> res = prefix(s);
  for (int i = 0; i < answ.size(); ++i) {
    EXPECT_EQ(res[i], answ[i]);
  }
  s = "abcdabcabcdabcdab";
  res = prefix(s);
  answ = {0, 0, 0, 0, 1, 2, 3, 1, 2, 3, 4, 5, 6, 7, 4, 5, 6};
  for (int i = 0; i < answ.size(); ++i) {
    EXPECT_EQ(res[i], answ[i]);
  }
}
TEST(Test_KMP, test_algo) {
  std::string p = "TEST";
  std::string s = "THIS IS A TEST TEXT";
  std::string res = kmp(p, s);
  EXPECT_EQ(res, "10 ");
  p = "AABA";
  s = "AABAACAADAABAAABAA";
  res = kmp(p, s);
  EXPECT_EQ("0 9 13 ", res);
}