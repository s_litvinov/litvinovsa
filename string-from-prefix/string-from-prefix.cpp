#include <iostream>
#include <vector>
#include <fstream>

std::string builtFromPrefix(const std::vector<int> &p) {
  char max = 0;
  std::string str = "a";
  for(int i = 1; i < p.size(); ++i){
    if (p[i] == 0){
      int j = i;
      max = str[p[j - 1]] + 1 ;
      while(p[j - 1] > 0){
        if( str[p[j - 1]] + 1 > max ){
          max = str[p[j -1]] + 1;
        }
        j = p[j - 1];
      }
      str+= max;
    }
    else
      str += str[p[i] - 1];
  }
  return str;
}

#ifndef GTEST_TEST
int main(int argc, char *argv[]){
  std::vector<int> input;
  std::ifstream fin("input.txt");
  int t;
  while(fin >> t)
    input.push_back(t);
  std::cout << builtFromPrefix(input) <<std::endl;
}
#endif