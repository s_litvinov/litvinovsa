#include <gtest/gtest.h>
#include "string-from-prefix/string-from-prefix.cpp"
#include "KMP_algo/kmp.cpp"
#include "z-func-to-string/z-func.cpp"

TEST(TestFunctions, test_prefix_func) {
  std::string s = "aabaaab";
  std::vector<int> answ = {0, 1, 0, 1, 2, 2, 3};
  std::vector<int> res = prefix(s);
  for (int i = 0; i < answ.size(); ++i) {
    EXPECT_EQ(res[i], answ[i]);
  }
  s = "abcdabcabcdabcdab";
  res = prefix(s);
  answ = {0, 0, 0, 0, 1, 2, 3, 1, 2, 3, 4, 5, 6, 7, 4, 5, 6};
  for (int i = 0; i < answ.size(); ++i) {
    EXPECT_EQ(res[i], answ[i]);
  }
}

TEST(TestFunctions, TestStringBuildFromPrefix) {
  EXPECT_EQ("abababa", builtFromPrefix(prefix("abababa")));
  EXPECT_EQ("abbabaabbabc", builtFromPrefix(prefix("abbabaabbabc")));
  EXPECT_EQ("aaaab", builtFromPrefix(prefix("aaaab")));
}

TEST(TestFunctions, TestTransformToPrefix) {
  std::vector<int> z = {5, 3, 2, 1, 0};
  EXPECT_EQ("aaaab", builtFromPrefix(PrefixBuildFromZ(z)));
  z = {11, 0, 0, 1, 0, 1, 0, 4, 0, 0, 1};
  EXPECT_EQ("abbacacabba", builtFromPrefix(PrefixBuildFromZ(z)));
  z = {0, 3, 2, 1};
  EXPECT_EQ("aaaa", builtFromPrefix(PrefixBuildFromZ(z)));
  z = {0, 0, 1, 0, 3, 0, 1};
  EXPECT_EQ("abacaba", builtFromPrefix(PrefixBuildFromZ(z)));
}