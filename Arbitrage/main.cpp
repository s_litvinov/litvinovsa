#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>

struct Edge {
  int vertex_one, vertex_two;
  double cost;
};
struct Graph {
  int vertexes;
  std::vector<Edge> edges;
};

bool FordBellman(Graph &graph, int vertex) {
  std::vector<double> d(graph.vertexes, 0);
  d[vertex] = 1;
  for (int i = 0; i < graph.vertexes - 1; ++i)
    for (int j = 0; j < graph.edges.size(); ++j)
      if (d[graph.edges[i].vertex_one > 0])
        d[graph.edges[j].vertex_two] = std::max(d[graph.edges[j].vertex_two],
                                                d[graph.edges[j].vertex_one] * graph.edges[j].cost);
  if (d[vertex] > 1)
    return true;
  return false;
}
void solve(Graph &graph, std::ostream *out) {
  bool arbitrage = false;
  int vertex;
  for (int i = 0; i < graph.vertexes; ++i) {
    vertex = i;
    if (FordBellman(graph, vertex)) {
      arbitrage = true;
      break;
    }
  }
  if (arbitrage)
    *out << "YES";
  else
    *out << "NO";
}
void parseGraph(Graph &graph, std::istream *in) {
  *in >> graph.vertexes;
  for (int i = 0; i < graph.vertexes; ++i)
    for (int j = 0; j < graph.vertexes; ++j) {
      Edge tmp;
      tmp.vertex_one = i;
      tmp.vertex_two = j;
      if (i == j)
        tmp.cost = 1;
      else
        *in >> tmp.cost;
      if (tmp.cost > 0)
        graph.edges.push_back(tmp);
    }
}

#ifndef GTEST_TEST
int main() {
  Graph graph;
  parseGraph(graph, &std::cin);
  solve(graph, &std::cout);
  return 0;
}
#endif