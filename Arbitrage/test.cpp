//
// Created by stanislav on 23.10.16.
//

#include "gtest/gtest.h"
#include "main.cpp"

TEST(TestAlgo, TestParsing) {
  Graph graph;
  std::stringstream input;
  input << "2 10.0 0.09";
  parseGraph(graph, &input);
  EXPECT_EQ(graph.vertexes, 2);
  EXPECT_EQ(graph.edges.size(), 4);
}

TEST(TestAlgo, TestSolver1) {
  Graph graph;
  std::stringstream input, out;
  input << "2 10.0 0.09";
  parseGraph(graph, &input);
  solve(graph, &out);
  EXPECT_EQ(out.str(), "NO");
}

TEST(TestAlgo, TestSolver2) {
  Graph graph;
  std::stringstream input, out;
  input << "4 32.1 1.52 78.66 0.03 0.04 2.43 0.67 21.22 51.89 0.01 -1 0.02";
  parseGraph(graph, &input);
  solve(graph, &out);
  EXPECT_EQ(out.str(), "YES");
}

TEST(TestAlgo, TestSolver3) {
  Graph graph;
  std::stringstream input, out;
  input << "3 0.67 -1 -1 78.66 0.02 -1";
  parseGraph(graph, &input);
  solve(graph, &out);
  EXPECT_EQ(out.str(), "YES");
}