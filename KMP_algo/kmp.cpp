#include<iostream>
#include <vector>
#include <string>

std::vector<int> prefix(const std::string &s) {
  std::vector<int> pref (s.length());
  pref[0] = 0;
  for (int i = 1, j = 0; i < s.length(); ++i) {
    while(j > 0 && s[i] != s[j])
      j = pref[j-1];
    if(s[i] == s[j])
      j++;
    pref[i] = j;
  }
  return pref;
}

std::string kmp(const std::string &pattern, const std::string &word) {
  std::string s = "";
  std::vector<int> f = prefix(pattern + "#" + word);
  for(unsigned long i = pattern.length()+1; i < f.size(); ++i)
    if (f[i] == pattern.length()) {
      s += std::to_string(i - 2 * pattern.length()) + " ";
    }
  return s;
}

#ifndef GTEST_TEST
int main(){
  std::string s1, s2;
  std::cin >> s1 >> s2;
  std::cout << kmp(s1, s2);
  return 0;
}
#endif