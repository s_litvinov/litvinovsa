#include <iostream>
#include <fstream>
#include <vector>
#include <queue>

int bfs(std::vector<std::vector<int> > &cap,
        std::vector<std::vector<int> > &f,
        int source,
        int target,
        unsigned nodes) {
  std::queue<int> q;
  q.push(source);
  std::vector<int> way(nodes);
  way[target] = -1;
  
  int curr;
  std::vector<int> flow;
  flow.assign(nodes, 0);
  flow[source] = 1001;
  while (way[target] == -1 && !q.empty()) {
    curr = q.front();
    q.pop();
    for (int i = 0; i < nodes; i++)
      if ((cap[curr][i] - f[curr][i]) > 0 && flow[i] == 0) {
        q.push(i);
        way[i] = curr;
        if (cap[curr][i] - f[curr][i] < flow[curr])
          flow[i] = (cap[curr][i] - f[curr][i]);
        else
          flow[i] = flow[curr];
      }
  }
  if (way[target] == -1) return 0;
  curr = target;
  while (curr != source) {
    f[way[curr]][curr] += flow[target];
    f[curr][way[curr]] -= flow[target];
    curr = way[curr];
  }
  return flow[target];
}

int search_max_flow(std::vector<std::vector<int> > &gr, int source, int target, unsigned int vertex_count) {
  std::vector<std::vector<int> > f(vertex_count);
  for (int i = 0; i < vertex_count; ++i) {
    f[i].assign(vertex_count, 0);
  }
  int MaxFlow = 0;
  int AddFlow;
  do {
    AddFlow = bfs(gr, f, source, target, vertex_count);
    MaxFlow += AddFlow;
  } while (AddFlow > 0);
  return MaxFlow;
}

int main() {
  int source, target, edges, nodes;
  std::vector<std::vector<int> > cap;
  for(;;) {
    std::cin >> nodes;
    if (nodes == 0)
      break;
    std::cin >> source >> target >> edges;
    cap.resize(nodes);
    for (int i = 0; i < nodes; ++i) {
      cap[i].assign(nodes, 0);
    }
    int from, to, flow;
    for (int i = 0; i < edges; ++i) {
      std::cin >> from >> to >> flow;
      cap[from - 1][to - 1] += flow;
      cap[to - 1][from - 1] += flow;
    }
    std::cout << search_max_flow(cap, source - 1, target - 1, nodes);
    std::cout << ' ';
  }
  return 0;
}