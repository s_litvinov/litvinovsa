//#include <iostream>
//#include <cstring>
//#include <queue>
//#include <cmath>
//
//const int maxVertex = 200;
//
//int nodes;
//
//int f[maxVertex][maxVertex];
//int cap[maxVertex][maxVertex];
//
//int flow[maxVertex];
//int ptr[maxVertex];
//int q[maxVertex];
//int qp, qc;
//
//int bfs(int source, int target) {
//  qp = 0;
//  qc = 1;
//  q[0] = source;
//  ptr[target] = -1;
//  int curr;
//  memset(flow, 0, sizeof(int) * nodes);
//  flow[source] = INFINITY;
//  while (ptr[target] == -1 && qp < qc) {
//    curr = q[qp];
//    for (int i = 0; i < nodes; i++)
//      if ((cap[curr][i] - f[curr][i]) > 0 && flow[i] == 0) {
//        q[qc] = i;
//        qc++;
//        ptr[i] = curr;
//        if (cap[curr][i] - f[curr][i] < flow[curr])
//          flow[i] = cap[curr][i];
//        else
//          flow[i] = flow[curr];
//      }
//    qp++;
//
//  }
//
//  if (ptr[target] == -1) return 0;
//  curr = target;
//  while (curr != source) {
//    f[ptr[curr]][curr] += flow[target];
//    curr = ptr[curr];
//  }
//  return flow[target];
//}
//
//int maxFlow(int source, int target) {
//  memset(f, 0, sizeof(int) * maxVertex * maxVertex);
//  int MaxFlow = 0;
//  int AddFlow;
//  do {
//    AddFlow = bfs(source, target);
//    MaxFlow += AddFlow;
//  } while (AddFlow > 0);
//  return MaxFlow;
//}
//
//int main() {
//  int edges, source, sink;
//  for (;;) {
//    std::cin >> nodes;
//    if (nodes == 0)
//      break;
//    for (int j = 0; j < maxVertex; ++j) {
//      for (int i = 0; i < maxVertex; ++i) {
//        cap[i][j] = 0;
//      }
//    }
//    std::cin >> source >> sink >> edges;
//    for (int i = 0; i < edges; ++i) {
//      int a, b, capacity;
//      std::cin >> a >> b >> capacity;
//      cap[a - 1][b - 1] += capacity;
//      cap[b - 1][a - 1] += capacity;
//    }
//    std::cout << maxFlow(source -1 , sink - 1) << std::endl;
//  }
//}
