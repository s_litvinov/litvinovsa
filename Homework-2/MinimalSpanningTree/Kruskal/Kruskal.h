//
// Created by stanislav on 15.12.16.
//

#ifndef HOMEWORK_2_KRUSKAL_H
#define HOMEWORK_2_KRUSKAL_H

struct Edge {
  int weight;
  int vrtx_one;
  int vrtx_two;
  Edge(int v1, int v2, int w) :
      vrtx_one(v1), vrtx_two(v2), weight(w) {}
};

class Graph {
 public:
  void makeGraph(int n);
  void add_edge(int v1, int v2, int w);
  int find_MST();
 private:
  int vertex_num;
  std::vector<Edge> edges;
  std::vector<int> parent;
  std::vector<int> rank;
  void make_set(int vertex);
  int find_set(int vertex);
  void union_sets(int a, int b);
};


#endif //HOMEWORK_2_KRUSKAL_H
