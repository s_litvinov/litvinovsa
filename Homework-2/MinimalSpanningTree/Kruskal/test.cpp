#include <gtest/gtest.h>
#include "init.cpp"

TEST(test1, test1) {
  EXPECT_EQ(1, 1);
}

TEST(GraphTest, GeneralTest) {
  std::stringstream fin;
  fin << "4 4 1 2 1 2 3 2 3 4 5 4 1 4";
  int vertexes, edges;
  fin >> vertexes >> edges;
  Graph g;
  g.makeGraph(vertexes);
  int a, b, weight;
  for (int i = 0; i < edges; ++i) {
    fin >> a >> b >> weight;
    g.add_edge(a, b, weight);
  }
  EXPECT_EQ(g.find_MST(), 7);
}