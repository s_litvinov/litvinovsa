//
// Created by stanislav on 15.12.16.
//

#include <algorithm>
#include "Kruskal.h"

void Graph::makeGraph(int n) {
  parent.resize(n);
  rank.resize(n, -1);
  vertex_num = n;
}
void Graph::union_sets(int a, int b) {
  a = find_set(a);
  b = find_set(b);
  if (a != b) {
    if (rank[a] < rank[b])
      std::swap(a, b);
    parent[b] = a;
    if (rank[a] == rank[b])
      ++rank[a];
  }
}
void Graph::make_set(int vertex) {
  parent[vertex] = vertex;
  rank[vertex] = 0;
}
int Graph::find_set(int vertex) {
  if (vertex == parent[vertex])
    return vertex;
  return parent[vertex] = find_set(parent[vertex]);
}
void Graph::add_edge(int v1, int v2, int w) {
  Edge new_edge(v1 - 1, v2 - 1, w);
  edges.push_back(new_edge);
}
int Graph::find_MST() {
  int MST_weight = 0;
  std::sort(edges.begin(), edges.end(), [](Edge a, Edge b) {
    return a.weight < b.weight;
  });
  for (int i = 0; i < vertex_num; ++i)
    make_set(i);
  for (int i = 0; i < edges.size(); ++i) {
    if (find_set(edges[i].vrtx_one)
        !=
            find_set(edges[i].vrtx_two)) {
      union_sets(edges[i].vrtx_one, edges[i].vrtx_two);
      MST_weight += edges[i].weight;
    }
  }
  return MST_weight;
}