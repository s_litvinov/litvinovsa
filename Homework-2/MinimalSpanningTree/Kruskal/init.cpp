#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "Kruskal.h"

#ifndef GTEST_TEST
int main() {
  std::ifstream fin("kruskal.in");
  std::ofstream fout("kruskal.out");
  int vertexes, edges;
  fin >> vertexes >> edges;
  Graph g;
  g.makeGraph(vertexes);
  int a, b, weight;
  for (int i = 0; i < edges; ++i) {
    fin >> a >> b >> weight;
    g.add_edge(a, b, weight);
  }
  fout << g.find_MST() << std::endl;
}
#endif